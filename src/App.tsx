import React from 'react';
import './App.css';
import { Calculator } from "./components/calculator.component"


function App() {


  return (
    <div className="App">
      <header className="App-header">
        <h1>NHB Max Mortgage Calculator</h1>
      </header>
      <Calculator></Calculator>

    </div>
  );
}

export default App;
