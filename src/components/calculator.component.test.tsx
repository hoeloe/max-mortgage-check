import { render, screen, fireEvent } from '@testing-library/react';
import { Calculator } from '../components/calculator.component';
import '@testing-library/jest-dom';
import axios from 'axios'


jest.mock('axios', () => {
    return {get: () => {
        return new Promise((resolve, reject) => {
            resolve({result: 10000})
        })
    }}
})


test('api call made after clicking', () => {
    const testValue = '10000'
    const calc = render(<Calculator></Calculator>)
    const input = screen.getByTestId('calc-1');
    const getSpy = jest.spyOn(axios, 'get')

    fireEvent.change(input, {target: {value: testValue}})
    fireEvent.click(screen.getByText('Calculate!'))
    expect(getSpy).toBeCalledTimes(1)

    const arg2 = getSpy.mock.calls[0][1]?.params
    let resultValue = 0
    if (arg2?.hasOwnProperty('person[0][income]')) {
        resultValue = arg2['person[0][income]']
    }
    expect(resultValue).toEqual(testValue)

})
