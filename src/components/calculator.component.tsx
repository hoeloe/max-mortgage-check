import React from 'react'
import * as ApiService from '../services/api-service'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Result from './result.component';


export class Calculator extends React.Component {


    state = {
            income: 0,
            maxMortgage: 0,
            objectValue: 0,
            studentLoans: 0,
            studentLoanStartDate: '2000-01-01',
            loans: 0,
        }

    render() {
        const {income, maxMortgage} = this.state
        return (
            <div className="calculator">
              <Result amount={maxMortgage} ></Result>
              <div className="by-income">
                  <h3>Calculate by income</h3>
                  <div className="input-holder">
                      <span>Yearly Income:</span>
                      <input data-testid='calc-1' type="number" onChange={this.setProp('income')}/>
                  </div>
                  <div className="input-holder">
                      <span>Student loan startdate:</span>
                      <input type="date" onChange={this.setProp('studentLoansStartDate')}/>
                  </div>
                  <div className="input-holder">
                      <span>Total Studentloans:</span>
                      <input type="number" onChange={this.setProp('studentLoans')}/>
                  </div>
                  <div className="input-holder">
                      <span>Loans:</span>
                      <input type="number" onChange={this.setProp('loans')}/>
                  </div>
                  <button onClick={this.calculateByIncome}>Calculate!</button>
              </div>
              <div className="by-value">
                  <h3>Calculate by Value</h3>
                  <div className="input-holder">
                      <span>Value of the object:</span>
                      value:
                      <input type="number" defaultValue={income} onChange={this.setProp('objectValue')}/>
                  </div>
                  <button onClick={this.calculateByValue}>Calculate By Value!</button>
              </div>
              <ToastContainer />
            </div>
        )
    }

    calculateByIncome = (): void => {
        const params = {"person[0][age]": 18,
                "person[0][income]": this.state.income,
                "person[0][loans]": this.state.loans,
                "person[0][studentLoans]": this.state.studentLoans,
                "person[0][studentLoanStartDate]": this.state.studentLoanStartDate,
        }
        ApiService.getMaxByIncome(params).then(
            (res: any) => this.setState({maxMortgage: res.data.data.result})
        ).catch(err => this.handleError(err))
    }

    calculateByValue = (): void => {
        ApiService.getMaxByValue(this.state.objectValue).then(
            (res: any) => this.setState({ maxMortgage: res.data.data.result })
        ).catch(err => this.handleError(err))
    }

    handleError = (error: any): void => {
        if (error.hasOwnProperty('response')) {
            toast.error(error.response?.data?.error?.message)
        } else {
            toast.error('Something went wrong')
        }
    }

    setProp = (fieldName: string) =>
        (e: React.ChangeEvent<HTMLInputElement>) => {
            this.setState({ [fieldName]: e.target.value})
    }

}
