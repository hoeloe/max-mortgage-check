import React from 'react'


export default class Result extends React.Component <{amount: number}> {


    state = {
        classNames: '',
    }

    removeAnimationClass = () => {
            this.setState({classNames: ''})
    }

    addAnimationClass = () => {
            this.setState({classNames: 'animate'})
    }

    componentDidUpdate (prevProps: any) {
        if (this.props.amount != prevProps.amount) {
            this.addAnimationClass()
        }
    }

    render() {
        const { classNames } = this.state
        return (
            <div className="result-holder">
              <h2>Your Maximum Available Mortgage is:</h2>
              <h1
                className={'result ' + classNames}
                onAnimationEnd={this.removeAnimationClass}
                    > {'€ '+(this.props.amount.toFixed(2))}</h1>
            </div>
        )
    }
}
