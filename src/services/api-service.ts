import axios from "axios"


const baseUrl = "https://api.hypotheekbond.nl/"

const apiKey = "91e82ff2-8e0d-469e-aaff-ecc67214cd3a"


export const getMaxByIncome = (givenParams: object): Promise<any> => {
    const baseParams = {
        nhg: false,
        private_lease_duration: 0,
        duration: 360,
        percentage: 1.501,
        rateFixation: 10,
    }
    const combined = {...baseParams, ...givenParams}
    return makeGetRequest("calculation/v1/mortgage/maximum-by-income", combined)
}

export const getMaxByValue = (value: number): Promise<any> => {
    const params = {
        objectvalue: value
    }
    return makeGetRequest("calculation/v1/mortgage/maximum-by-value", params)
}

const makeGetRequest = (path: string, params: object) => {
    let config = {
        params: {...params, api_key: apiKey}
    }
    return axios.get<any>(baseUrl + path, config)
}
